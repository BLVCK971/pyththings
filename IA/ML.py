# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 22:43:42 2019

@author: Mirror
"""
import pandas as pd
import numpy as np
# Exportation de mon tableau
dataset = pd.read_json('Data/MatchsPays/Reduced/all.json')

# Selection de mes variables d'entrées et de sortie
X = dataset.iloc[:, 0:4]
y = dataset.iloc[:, -1]
ref = y

from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.compose import make_column_transformer
preprocess = make_column_transformer(
    (OneHotEncoder(),['home','away','competition']),(StandardScaler(), ['date']))
X = preprocess.fit_transform(X)


y = y.values
from sklearn.preprocessing import LabelBinarizer
lb_y = LabelBinarizer()
y = lb_y.fit_transform(y)

# Définition des données d'entrainement et de tests
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, shuffle=True)

# Changement d'échelle pour avoir des nombre entre 0 et 1
# sc = StandardScaler(with_mean=False)
# X_train = sc.fit_transform(X_train)
# X_test = sc.fit_transform(X_test)


from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
# import datetime 
# from keras.callbacks import TensorBoard
# import os

classifier = Sequential() 
classifier.add(Dense(units=335, activation="relu",
                      kernel_initializer="uniform", input_dim=335))
classifier.add(Dense(units=3, activation="softmax",
                      kernel_initializer="uniform"))
# #tensorboard = TensorBoard(log_dir= os.path.join("logs","fit",datetime.datetime.now().strftime("%Y%m%d-%H%M%S")))

classifier.compile(optimizer=optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True), loss="categorical_crossentropy", 
                    metrics=["accuracy"])
# classifier.compile(optimizer="adam", loss="categorical_crossentropy", 
#                     metrics=["accuracy"])

#classifier.fit(X_train,y_train, batch_size=32,epochs=100, validation_data=(X_test,y_test), callbacks=[tensorboard])
classifier.fit(X_train,y_train, batch_size=32,epochs=100, validation_data=(X_test,y_test))

score_train = classifier.evaluate(X_train, y_train)
score_test = classifier.evaluate(X_test, y_test)


# y_pred=classifier.predict(X_test)
# y_pred = (y_pred >0.36)


# y_pred=np.argmax(y_pred, axis=1)

# y_test = (y_test >0.5)
# y_train = (y_train >0.5)
# y_test=np.argmax(y_test, axis=1)
# y_train=np.argmax(y_train, axis=1)

# from sklearn.metrics import confusion_matrix   
# cm = confusion_matrix(y_test, y_pred)

from win10toast import ToastNotifier
toaster = ToastNotifier()
toaster.show_toast("First Part",
"Ce n'est que le début",
duration=10, threaded=True)


# classifier.save('Test.h5')

# X_new = pd.read_json('Data/MatchsPays/Reduced/apredire.json')

# X_new = preprocess.transform(X_new)

# ynew = classifier.predict_proba(X_new, batch_size=10)
# proba = ynew
# ynew = lb_y.inverse_transform(ynew)


def build_classifier(dropoutrate, neurons, optimizer='adam'):
    classifier = Sequential() 
    classifier.add(Dense(units=neurons, activation="relu",
                          kernel_initializer="uniform", input_dim=335))
    classifier.add(Dropout(rate=dropoutrate))
    classifier.add(Dense(units=3, activation="softmax",
                          kernel_initializer="uniform"))
    classifier.compile(optimizer=optimizer, loss="categorical_crossentropy", 
                        metrics=["accuracy"])
    return classifier

from keras.wrappers.scikit_learn import KerasClassifier
# from sklearn.model_selection import cross_val_score
# classifier = KerasClassifier(build_fn=build_classifier, batch_size=10,epochs=100)
# accuracies = cross_val_score(estimator = classifier, X= X_train, y = y_train,
#                              cv = 10)

# mean = accuracies.mean()
# variance = accuracies.std()

toaster = ToastNotifier()
toaster.show_toast("La suite",
"Bonne chance",
duration=10, threaded=True)

from sklearn.model_selection import GridSearchCV
classifier = KerasClassifier(build_fn = build_classifier)
parameters = {'batch_size': [25, 32],
              'epochs': [100],
              'optimizer': ["adam", "rmsprop", 'SGD', 'Adagrad', 'Adadelta', 'Adamax', 'Nadam', optimizers.SGD(lr=0.001, decay=1e-3, momentum=0.9, nesterov=True) ],
              'dropoutrate' : [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9],
              'neurons' : [335, 300, 125 , 50]}
grid_search = GridSearchCV(estimator = classifier,
                           param_grid = parameters,
                           scoring = "accuracy",
                           cv = 10,
                           n_jobs=1)
grid_search = grid_search.fit(X_train, y_train,validation_data=(X_test,y_test))
best_parameters = grid_search.best_params_
best_accuracy = grid_search.best_score_


toaster.show_toast("Fini",
"Allez au boulot !",
duration=10, threaded=True)

## Entrant :
### homeTeam > name (ou id)
### awayTeam > name (ou id)
### competition > name
### utcDate :10

## Sortant :
### score > winner  HOME_TEAM , AWAY_TEAM , DRAW
                       
## Probable Entrant :
### utcDate

## Probable Sortant :
#### score > fullTime > homeTeam int
#### score > fullTime > awayTeam int