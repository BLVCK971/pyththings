# -*- coding: utf-8 -*-
from os import chdir
chdir('..')

import keras
import pandas as pd

class = keras.models.load_model('Test.h5')
ref = pd.read_json('Data/MatchsPays/Reduced/all.json')

xlsx = pd.read_excel('LotoFootData.xlsx', sheet_name='Prochains', usecols=[1,2,3,5])
dataset = pd.read_excel('LotoFootData.xlsx', sheet_name='Prochains', usecols=[3,5])

xlsx = xlsx.dropna()
dataset = dataset.dropna()

allxlsteams = pd.concat([xlsx["Column3"],xlsx["Column5"]]).unique()
allfootteams = pd.concat([ref["home"],ref["away"]]).unique()

