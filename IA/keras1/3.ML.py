# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 22:43:42 2019

@author: Mirror
"""
import pandas as pd

# Exportation de mon tableau
ref = pd.read_json('Reduced/all.json')
dataset = pd.read_json('Reduced/all.json')

# Selection de mes variables d'entrées et de sortie
X = dataset.iloc[:, 0:2].values
y = dataset.iloc[:, 2].values

# Transformation des catégories en nombres entiers
from sklearn.preprocessing import OneHotEncoder, StandardScaler


# =============================================================================
# from sklearn.preprocessing import LabelEncoder
# labelencoder_X_1 = LabelEncoder()
# X[:,0] = labelencoder_X_1.fit_transform(X[:,0])
# 
# labelencoder_X_2 = LabelEncoder()
# X[:,1] = labelencoder_X_2.fit_transform(X[:,1])
# 
# labelencoder_y = LabelEncoder()
# y = labelencoder_y.fit_transform(y)
# =============================================================================

# Transformation des nombres entiers en colonnes de catégories binaires
onehotencoder = OneHotEncoder()
X = onehotencoder.fit_transform(X).toarray()
y = onehotencoder.fit_transform(y.reshape(-1,1)).toarray()

# On enlève la première colonne car elle est explicite avec le reste 
#(quand tout est à 0 c'est la première)
X = X[:, 1:]
#y = y[:, 1:]

# Définition des données d'entrainement et de tests
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25 )

# Changement d'échelle pour avoir des nombre entre 0 et 1
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.fit_transform(X_test)

## Construction du Réseau
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
import datetime 
#from tensorflow.python.keras.callbacks import TensorBoard
from keras.callbacks import TensorBoard
import os
# Initialisation du RN
classifier = Sequential()

# Ajout d'une couche d'entrée et caché (entrée automatique)
classifier.add(Dense(units=327, activation="relu",
                     kernel_initializer="uniform", input_dim=327))


classifier.add(Dense(units=150, activation="relu",
                     kernel_initializer="uniform"))
classifier.add(Dense(units=100, activation="relu",
                     kernel_initializer="uniform"))


# couche couche de sortie
classifier.add(Dense(units=3, activation="softmax",
                     kernel_initializer="uniform"))

#tensorboard = TensorBoard(log_dir='logs/{}'.format(datetime.now().strftime("%d%m%Y%H%M%S")))
tensorboard = TensorBoard(log_dir= os.path.join("logs","fit",datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),))

classifier.compile(optimizer= optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True),loss="categorical_crossentropy", metrics=["accuracy"])

# classifier.compile(optimizer= "adam", 
#                    loss="categorical_crossentropy", 
#                    metrics=["accuracy"])

classifier.fit(X_train,y_train, batch_size=10,epochs=100, validation_data=(X_test,y_test), callbacks=[tensorboard])

y_pred=classifier.predict(X_test)
y_pred = (y_pred >0.10)
y_test = (y_test >0.5)


from win10toast import ToastNotifier
toaster = ToastNotifier()
toaster.show_toast("Fini",
"Python is 10 seconds awsm!",
duration=10, threaded=True)

classifier.save('Test.h5')

## Entrant :
### homeTeam > name (ou id)
### awayTeam > name (ou id)

## Sortant :
### score > winner  HOME_TEAM , AWAY_TEAM , DRAW

## Probable Entrant :
### utcDate

## Probable Sortant :
#### score > fullTime > homeTeam int
#### score > fullTime > awayTeam int