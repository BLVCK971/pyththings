# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 22:43:42 2019

@author: Mirror
"""
import pandas as pd


# Exportation de mon tableau
dataset = pd.read_json('Data/MatchsPays/Reduced/all.json')

# Selection de mes variables d'entrées et de sortie
X = dataset.iloc[:, 0:4]
y = dataset.iloc[:, -1]
ref = y

# # Transformation des catégories en nombres entiers
# # =============================================================================
# from sklearn.preprocessing import LabelEncoder

# labelencoder_X_1 = LabelEncoder()
# labelencoder_X_1.fit(X[:,0])
# X[:,0] = labelencoder_X_1.transform(X[:,0])

# labelencoder_X_2 = LabelEncoder()
# labelencoder_X_2.fit(X[:,1])
# X[:,1] = labelencoder_X_2.transform(X[:,1])

# labelencoder_X_3 = LabelEncoder()
# labelencoder_X_3.fit(X[:,3])
# X[:,3] = labelencoder_X_3.transform(X[:,3])

# # ============================================================================

# from sklearn.preprocessing import OneHotEncoder, StandardScaler
# # Transformation des nombres entiers en colonnes de catégories binaires


# ohe1 = OneHotEncoder()
# ohe1.fit(X[:,0])
# X[:,0] = ohe1.transform(X[:,0]).toarray()

# ohe3 = OneHotEncoder()
# ohe3.fit(X[:,3])
# X[:,3] = ohe3.transform(X[:,3]).toarray()

from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.compose import make_column_transformer
preprocess = make_column_transformer(
    (OneHotEncoder(),['home','away','competition']),
    (StandardScaler(), ['date']))
X = preprocess.fit_transform(X)


y = y.values
from sklearn.preprocessing import LabelBinarizer
lb_y = LabelBinarizer()
y = lb_y.fit_transform(y)

# Définition des données d'entrainement et de tests
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25 )

# Changement d'échelle pour avoir des nombre entre 0 et 1
sc = StandardScaler(with_mean=False)
X_train = sc.fit_transform(X_train)
X_test = sc.fit_transform(X_test)

## Construction du Réseau
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense
import datetime 
#from tensorflow.python.keras.callbacks import TensorBoard
from keras.callbacks import TensorBoard
import os

# Initialisation du RN
classifier = Sequential()

# Ajout d'une couche d'entrée et caché (entrée automatique)
classifier.add(Dense(units=335, activation="relu",
                     kernel_initializer="uniform", input_dim=335))

# couche couche de sortie
classifier.add(Dense(units=3, activation="softmax",
                     kernel_initializer="uniform"))

#tensorboard = TensorBoard(log_dir='logs/{}'.format(datetime.now().strftime("%d%m%Y%H%M%S")))
tensorboard = TensorBoard(log_dir= os.path.join("logs","fit",datetime.datetime.now().strftime("%Y%m%d-%H%M%S"),))



# classifier.compile(optimizer=optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9, nesterov=True), loss="categorical_crossentropy", 
#                    metrics=["accuracy"])
classifier.compile(optimizer="adam", loss="categorical_crossentropy", 
                    metrics=["accuracy"])
classifier.fit(X_train,y_train, batch_size=10,epochs=50, validation_data=(X_test,y_test), callbacks=[tensorboard])

y_pred=classifier.predict(X_test)
y_pred = (y_pred >0.36)
y_test = (y_test >0.5)

from sklearn.metrics import confusion_matrix   
cm = confusion_matrix(y_test.argmax(axis=1), y_pred.argmax(axis=1))

from win10toast import ToastNotifier
toaster = ToastNotifier()
toaster.show_toast("Fini",
"Python is 10 seconds awsm!",
duration=10, threaded=True)

classifier.save('Test.h5')

X_new = pd.read_json('Data/MatchsPays/Reduced/apredire.json')

# X_new = X_new.iloc[:,:].values
# X_new[:,2] = pd.to_datetime(X_new[:,2])
# X_new[:,0] = labelencoder_X_1.transform(X_new[:,0])

# X_new[:,1] = labelencoder_X_2.transform(X_new[:,1])

# X_new[:,3] = labelencoder_X_3.transform(X_new[:,3])
# print(X_new)

# X_new = onehotencoder1.transform(X_new).toarray()


X_new = preprocess.transform(X_new)

ynew = classifier.predict_proba(X_new)

## Entrant :
### homeTeam > name (ou id)
### awayTeam > name (ou id)
### competition > name
### utcDate :10

## Sortant :
### score > winner  HOME_TEAM , AWAY_TEAM , DRAW

## Probable Entrant :
### utcDate

## Probable Sortant :
#### score > fullTime > homeTeam int
#### score > fullTime > awayTeam int