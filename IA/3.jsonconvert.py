import glob
import json


# for file in glob.glob('Data/MatchsPays/*.json'):
# 	with open('Data/MatchsPays/' + file.split('\\')[-1]) as matches :
# 		data_dict = json.load(matches)
# 		print(len(data_dict))


### Affichage nom équipes et gagnant
# with open('Data/MatchsPays/France.json') as matches :
#  		data_dict = json.load(matches)
#  		for i in range(len(data_dict)):
# 	 		print(data_dict[i]['homeTeam']['name'])
# 	 		print('vs')
# 	 		print(data_dict[i]['awayTeam']['name'])
# 	 		print()
# 	 		if(data_dict[i]['score']['winner']=='HOME_TEAM'):
# 	 			print('winner is :' + data_dict[i]['homeTeam']['name'])
# 	 		elif (data_dict[i]['score']['winner']=='AWAY_TEAM'):
# 	 			print('winner is : ' + data_dict[i]['awayTeam']['name'])
# 	 		else:
# 	 			print('DRAW')
# 	 		print()

match_tab = []

for file in glob.glob('Data/MatchsPays/*.json'):
    with open('Data/MatchsPays/' + file.split('\\')[-1]) as matches :
        data_dict = json.load(matches)
        for i in range(len(data_dict)):
            match = {}
            match['home'] = data_dict[i]['homeTeam']['name']
            match['away'] = data_dict[i]['awayTeam']['name']
            match['date'] = data_dict[i]['utcDate'][:10]
            match['competition'] = data_dict[i]['competition']['name']
            if(data_dict[i]['score']['winner']=='HOME_TEAM'):
                match['win'] = '1'
            elif (data_dict[i]['score']['winner']=='AWAY_TEAM'):
                match['win'] = '2'
            else:
                match['win'] = 'N'
            match_tab.append(match)
            
print(match_tab)
with open('Data/MatchsPays/Reduced/all.json','w') as f :
		f.write(json.dumps(match_tab, indent=4))



## Entrant :
### homeTeam > name (ou id)
### awayTeam > name (ou id)

## Sortant :
### score > winner  HOME_TEAM , AWAY_TEAM , DRAW

## Probable Entrant :
### utcDate

## Probable Sortant :
#### score > fullTime > homeTeam int
#### score > fullTime > awayTeam int