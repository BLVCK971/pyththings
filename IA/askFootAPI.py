import requests
import json
from datetime import date, timedelta
import time
import os,sys
try :
	from playsound import playsound
except:
	
os.system('cls')

api_token = "c5b7c3204d0944db8e9049383efffe32"
api_url_base = "https://api.football-data.org/v2/"
headers = {'X-Auth-Token': api_token}


# 10 requêtes par minutes


date_debut = date(2017,1,1)
date_fin = (date.today() - timedelta(days=1))


pays = { 
'France' : '2015',
'Angleterre' : '2021',
'Espagne' : '2014',
'Italie' : '2019',
'Portugal' : '2017',
'Allemagne' :'2002' } 

limit = 0

for payss, code in pays.items() :
	date_ref = date_debut
	date_ref_ten = date_ref + timedelta(days=10)
	tabb = []
	while date_ref < date_fin :
		target = 'matches?competitions=' + code + '&dateFrom=' + date_ref.strftime("%Y-%m-%d") + '&dateTo=' + date_ref_ten.strftime("%Y-%m-%d") + '&status=FINISHED'
		r = requests.get(api_url_base + target , headers=headers)
		data = json.loads(r.text)
		try:
			if data['matches'] != []:
				print(payss + " " + '%.1f'%(((date_ref-date_debut)/(date_fin - date_debut))*100) + '%',end='\r')
				tabb.extend(data['matches'])
		except:
			print("Limite des requêtes atteinte ! Attendez 1 minute !!!")
			sys.exit()
		date_ref = date_ref_ten
		date_ref_ten = date_ref + timedelta(days=10)
		limit += 1
		if limit == 10:
			
			print('[PAUSE]',end='\r')
			print(..., end="")
			time.sleep(61)
			limit = 0
	print(payss+ ' DONE !!!')
	print('[CHANGEMENT DE PAYS]')
	with open('Data/MatchsPays/' + payss + '.json','a') as f :
		f.write(json.dumps(tabb, indent=4))
	time.sleep(61)

playsound('Hamza.mp3')

### score > winner
###	score > homeTeam (score)
### score > awayTeam (score)