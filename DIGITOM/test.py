# -*- coding: utf-8 -*-
import functools
import xmlrpc.client

HOST = 'localhost'
PORT = 8000
DB = 'db_test'
USER = 'admin'
PASS = 'admin'
ROOT = 'http://%s:%d/xmlrpc/'%(HOST,PORT)

uid = xmlrpc.client.ServerProxy(ROOT + 'common').login(DB, USER, PASS)
print ("Logged in as %s (uid: %d)" % (USER,uid))

call = functools.partial(
	xmlrpc.client.ServerProxy(ROOT + 'object').execute,
	DB, uid, PASS)

sessions = call('squelette.session','search_read',[],['name','seats'])
for session in sessions:
	print("Sessions %s (%s seats)" % (session['name'], session['seats']))

	session_id = call('squelette.session', 'create', {
		'name' : 'My session',
		'course_id' : 2,
		})