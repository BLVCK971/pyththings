#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import logging, os, glob
from logging.handlers import RotatingFileHandler
from datetime import datetime, timedelta
from shutil import copytree
import tarfile

#def replacement():
#	os.chdir("..")
#	os.chdir("..")

dbname = ""
#cr.dbname


def initlog():

	logger = logging.getLogger()
	logger.setLevel(logging.INFO)
 
	formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

	# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
	file_handler = RotatingFileHandler('logs/' + datetime.now().strftime("%Y%m%d-%H-%M-%S") + '.log', 'a', 1000000, 1)

	file_handler.setLevel(logging.INFO)
	file_handler.setFormatter(formatter)
	logger.addHandler(file_handler)
	return logger

def suppr_old_logs(tp):
	d = datetime.today() - timedelta(days=tp)
	for file in glob.glob("logs/"+ str(d.strftime("%Y%m%d")) +"*"):
		lg.info(file + ' removed')
		os.remove(file) 

lg = initlog()
lg.info('Start Log')

suppr_old_logs(7)

if not os.path.isdir('temp'):
	os.mkdir('temp')

if os.path.isdir('filestore') :
	tf = tarfile.open("temp/sample.tar.gz", mode="w:gz")
	for file in glob.glob("filestore/*"):
		lg.info(file + ' compressed')
		tf.add(file)
	
	tf.close()	
else:	
	lg.error('No Filestore !')


