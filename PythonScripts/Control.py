from tkinter import *
import os, sys, subprocess
win = Tk()

b1 = Button(win, text = "SHUTDOWN", command = lambda: subprocess.call(["shutdown.exe", "-f", "-s", "-t", "0"]))
b2 = Button(win, text = "RESTART", command = lambda: subprocess.call(["shutdown.exe", "-f", "-r", "-t", "0"]))


l = Label(win, text = "Apps")
k = Label(win, text = "Games")
j = Label(win, text = "Misc")

l.grid(row = 0, column = 0, padx = 10, pady = 10)
k.grid(row = 0, column = 1, padx = 10, pady = 10)
j.grid(row = 0, column = 2, padx = 10, pady = 10)

b1.grid(row = 1, column = 0, padx = 10, pady = 10)
b2.grid(row = 2, column = 0, padx = 10, pady = 10)

mainloop()