
### Vérifier existence d'un dossier
# import os
# print(os.path.isdir('library/dat'))



### Vérifier espace Disque
# import shutil
# total, used, free = shutil.disk_usage("/")

# print("Total: %d Go" % (total // (2**30)))
# print("Utilisé: %d Go" % (used // (2**30)))
# print("Libre: %d Go" % (free // (2**30)))



### Date module
# from datetime import datetime
# #print(datetime.now().strftime("%Y%m%d-%H:%M:%S"))
# print(datetime.now().strftime("%Y%m%d"))
# today = date.today().strftime("%d-%m-%Y")

### Déplacement dossiers 
# #import os
# dossierdepart = os.getcwd()
# os.chdir("..")
# #do stuff in parent directory
# os.chdir(dossierdepart)     # go back where you came from

### FTP Gestion
# import ftplib as ftp
# def ftptransfer
# # Infos FTP
# 	host = "votre_host.com" # adresse du serveur FTP
# 	user = "votre_pseudo" # votre identifiant
# 	password = "votre password" # votre mot de passe
# 	connect = ftp.ftplib(host,user,password) # on se connecte

### Mini fonction de création et de vérification de dossier pour BACKUP_DIR
# for n in BACKUP_DIR.split('/'):
# 	if path == '':
# 		path = n
# 	else:
# 		path = path +'/'+ n
# 	if !os.path.isdir(path):
# 		os.mkdir(path)


### Exploration de dossier
# import os

# for _,_,z in os.walk('.'):
# 	print(z)
# 	for n in z:
# 		print(n)